from gasp import *

begin_graphics()
def drawFace(x,y):
    Circle((200 - x, 200 - y), 40)
    Circle((185 - x, 220 - y), 5)
    Circle((215 - x, 220 - y), 5)
    Line((190 - x, 190 - y), (210 - x, 190 - y))
    Line((190 - x, 190 - y), (200 - x, 230 - y))
    Line((180 - x, 230 - y), (190 - x, 230 - y))
    Line((210 - x, 230 - y), (220 - x, 230 - y))
    Arc((200 - x, 200 - y), 30, 225, 315)
    Arc((165 - x, 200 - y), 20, 90, 270)
    Arc((235 - x,200 - y), 20, 270, 450)

drawFace(100,20)
update_when('key_pressed')      # you know what this does by now...
end_graphics()

